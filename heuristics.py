from board import Board

NEXT_MOVE_ADDON = 15
BEATING_ADDON = 15


def scores_subtraction(board: Board):
    return board.opponent_points - board.player_points \
        if board.reversed \
        else board.player_points - board.opponent_points


def force_next_move(board: Board):
    addon = 0 if board.are_no_more_moves else NEXT_MOVE_ADDON
    return scores_subtraction(board) + addon


def force_beating(board: Board):
    addon = 0 if board.has_beating else BEATING_ADDON
    return scores_subtraction(board) + addon
