from board import Board
import heuristics
import random
import time

DEPTH = 4


def player_move(board: Board):
    has_move_continue = True
    while has_move_continue:
        try:
            command = input("\n\nPlayer move: ").split()
            field_id = int(command[0])
            has_move_continue = board.make_player_move(field_id - 1)
            board.print_board()
        except:
            print("Wrong move: ", command[0])
            continue
    return board.get_opponent_board()


def bot_move(board: Board, depth: int, alpha_beta: bool):
    best_ids_sequence = board.find_best_move(depth, alpha_beta)
    for field_id in best_ids_sequence:
        board.make_player_move(field_id - 1)
    return board.get_opponent_board(), len(best_ids_sequence)


def random_move(board: Board):
    while True:
        field_id = random.randint(1, 6)
        if not board.make_player_move(field_id - 1):
            break
    return board.get_opponent_board()


def run_player_vs_bot(player_starts=True):
    board = Board()
    board.print_board()

    while True:
        if player_starts:
            board = player_move(board)
            if board.are_no_more_moves:
                break

            board = bot_move(board)
            if board.are_no_more_moves:
                break
        else:
            board = bot_move(board)
            if board.are_no_more_moves:
                break

            board = player_move(board)
            if board.are_no_more_moves:
                break
    board.get_opponent_board().print_game_result()


def run_bot_vs_bot(
        heuristic,
        first_depth,
        second_depth,
        first_alpha_beta,
        second_alpha_beta
):
    first_player_time = 0
    second_player_time = 0

    first_player_moves = 0
    second_player_moves = 0

    board = Board(None, heuristic)
    board = random_move(board)
    first_player_moves += 1

    while True:
        # reversed, second
        start_time = time.time()
        board, moves = bot_move(board, second_depth, second_alpha_beta)
        second_player_time += time.time() - start_time
        second_player_moves += moves
        if board.are_no_more_moves:
            break

        start_time = time.time()
        board, moves = bot_move(board, first_depth, first_alpha_beta)
        first_player_time += time.time() - start_time
        first_player_moves += moves
        if board.are_no_more_moves:
            break

    winning_time = second_player_time if board.reversed else first_player_time
    winning_moves = second_player_moves if board.reversed else first_player_moves
    return winning_time, winning_moves


if __name__ == "__main__":
    # run_player_vs_bot()

    games = 1
    winning_times = []
    winning_moves_counts = []

    for _ in range(games):
        winning_time, winning_moves = run_bot_vs_bot(
            heuristic=heuristics.force_beating,
            first_depth=4,
            second_depth=4,
            first_alpha_beta=False,
            second_alpha_beta=True
        )
        winning_times.append(winning_time)
        winning_moves_counts.append(winning_moves)

    avg_time = round(sum(winning_times) / games, 2)
    avg_moves = round(sum(winning_moves_counts) / games, 2)
    print("Avg winning time:", str(avg_time) + "s")
    print("Avg winning moves:", str(avg_moves))
