from typing import Callable
import math


class Board:
    PLAYER_WELL_ID = 7

    __board: list = None
    __reversed: bool = None
    __heuristic: Callable = None
    __has_beating = False

    def __init__(self, board=None, heuristic=None):
        if board:
            self.__board = list(board.__board)
            self.__reversed = board.__reversed
            self.__heuristic = board.__heuristic
        else:
            self.__board = [0, 4, 4, 4, 4, 4, 4, 0, 4, 4, 4, 4, 4, 4]
            self.__reversed = False
            self.__heuristic = heuristic

    def make_player_move(self, field_id):
        field_id += 1
        tokens = self.__board[field_id]

        if self.are_no_more_moves or tokens == 0:
            return False

        self.__board[field_id] = 0

        while tokens:
            tokens -= 1
            field_id += 1
            if field_id >= len(self.__board):
                field_id = 0

            self.__board[field_id] += 1

        if field_id == self.PLAYER_WELL_ID:
            return True

        if self.__board[field_id] == 1 and 0 < field_id < 7:
            opposite_field_id = len(self.__board) - field_id
            if self.__board[opposite_field_id] != 0:
                self.__board[field_id] = 0
                self.__board[self.PLAYER_WELL_ID] += 1 + self.__board[opposite_field_id]
                self.__board[opposite_field_id] = 0
                self.__has_beating = True
        return False

    def get_opponent_board(self):
        board = Board()
        board.__board = self.__board[7:] + self.__board[:7]
        board.__reversed = not self.__reversed
        board.__heuristic = self.__heuristic
        return board

    def __get_available_moves(self):
        available_moves = []
        for field_id in self.__possible_player_move_ids:
            self.__append_available_moves(field_id, [], available_moves)
        return available_moves

    def __append_available_moves(self, field_id, field_ids_sequence, available_moves):
        board_copy = Board(self)
        has_move_continue = board_copy.make_player_move(field_id)

        if has_move_continue and len(board_copy.__possible_player_move_ids):
            for continue_field_id in board_copy.__possible_player_move_ids:
                board_copy.__append_available_moves(
                    continue_field_id,
                    field_ids_sequence + [field_id],
                    available_moves
                )
        else:
            available_moves.append((
                field_ids_sequence + [field_id],
                board_copy
            ))
            return

    @property
    def __possible_player_move_ids(self):
        return [
            field_id
            for field_id, tokens in enumerate(self.__board[1:7])
            if tokens > 0
        ]

    def find_best_move(self, depth=3, alpha_beta=False):
        cur_best_score = -999
        cur_best_solution = None

        for field_ids_sequence, board in self.__get_available_moves():
            new_score = board.__alpha_beta(depth, False, -math.inf, math.inf) \
                if alpha_beta \
                else board.__min_max(depth, False)

            if new_score > cur_best_score:
                cur_best_score = new_score
                cur_best_solution = [y + 1 for y in field_ids_sequence]
        return cur_best_solution

    def __min_max(self, depth, maximizing_player=False):
        if depth == 0 or self.are_no_more_moves:
            return self.__min_max_score

        cur_best_score = -999 if maximizing_player else 999
        for _, board in self.get_opponent_board().__get_available_moves():
            score = board.__min_max(depth - 1, not maximizing_player)
            cur_best_score = max(cur_best_score, score) if maximizing_player else min(cur_best_score, score)
        return cur_best_score

    def __alpha_beta(self, depth, maximizing_player: bool, alpha: int, beta: int):
        if depth == 0 or self.are_no_more_moves:
            return self.__min_max_score

        cur_best_score = -999 if maximizing_player else 999
        for _, board in self.get_opponent_board().__get_available_moves():
            score = board.__alpha_beta(depth - 1, not maximizing_player, alpha, beta)
            cur_best_score = max(cur_best_score, score) if maximizing_player else min(cur_best_score, score)
            if maximizing_player:
                if cur_best_score >= beta:
                    return cur_best_score
                alpha = max(alpha, cur_best_score)

            if not maximizing_player:
                if cur_best_score <= alpha:
                    return cur_best_score
                beta = min(beta, cur_best_score)

        return cur_best_score

    @property
    def __min_max_score(self):
        return self.__heuristic(self)

    @property
    def player_points(self):
        if self.are_no_more_moves:
            return sum(self.__board[1:8])
        else:
            return self.__board[self.PLAYER_WELL_ID]

    @property
    def opponent_points(self):
        if self.are_no_more_moves:
            return self.__board[0] + sum(self.__board[8:])
        else:
            return self.__board[0]

    @property
    def reversed(self):
        return self.__reversed

    @property
    def are_no_more_moves(self):
        if any(self.__board[8:]) is False or any(self.__board[1:7]) is False:
            return True
        return False

    @property
    def has_beating(self):
        return self.__has_beating

    def print_board(self):
        print("         ", end="")
        print("")
        print("moves:      6   5   4   3   2   1")
        print("")
        print("         ", end="")

        print(*["\t%1d" % x for x in reversed(self.__board[8:])], sep="|")
        print(
            "enemy --> %2d                      %2d <-- player"
            % (self.opponent_points, self.player_points)
        )
        print("         ", end="")
        print(*["\t%1d" % x for x in self.__board[1:7]], sep="|")
        print("")
        print("moves:      1   2   3   4   5   6")

    def print_game_result(self):
        print("First player points:", self.player_points)
        print("Second player points:", self.opponent_points)
        if self.player_points > self.opponent_points:
            print("First player won!")
        elif self.player_points < self.opponent_points:
            print("Second player won!")
        else:
            print("draw")
        print("Games ended")
